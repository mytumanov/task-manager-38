package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository(connectionService);

    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private List<Task> bindedTasks;

    @NotNull
    private List<Task> unbindedTasks;

    @NotNull
    private static final Project project = new Project();

    @Before
    public void initRepository() throws Exception {
        bindedTasks = new ArrayList<>();
        unbindedTasks = new ArrayList<>();
        project.setName("PROJECT");
        project.setDescription("PROject description");
        project.setUserId(USER_ID);
        projectRepository.add(project);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task binded name: " + i);
            task.setDescription("Task binded description: " + i);
            task.setUserId(USER_ID);
            task.setProjectId(project.getId());
            taskRepository.add(task);
            bindedTasks.add(task);
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task unbinded name: " + i);
            task.setDescription("Task unbinded description: " + i);
            task.setUserId(USER_ID);
            taskRepository.add(task);
            unbindedTasks.add(task);
        }
    }

    @After
    public void clearRepository() throws Exception {
        taskRepository.clear();
        projectRepository.clear();
        bindedTasks.clear();
        unbindedTasks.clear();
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        for (@NotNull final Task task : unbindedTasks) {
            assertNull(task.getProjectId());
            projectTaskService.bindTaskToProject(USER_ID, project.getId(), task.getId());
            @NotNull final Task actualTask = taskRepository.findOneById(task.getId());
            assertEquals(project.getId(), actualTask.getProjectId());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER_ID, "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER_ID, "123", "");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testBadUseryBindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        assertNotEquals(0, taskRepository.findAllByProjectId(USER_ID, project.getId()).size());
        assertTrue(projectRepository.existById(project.getId()));
        projectTaskService.removeProjectById(USER_ID, project.getId());
        assertFalse(projectRepository.existById(project.getId()));
        assertEquals(0, taskRepository.findAllByProjectId(USER_ID, project.getId()).size());
    }

    @Test(expected = IdEmptyException.class)
    public void testProjectEmptyRemoveProjectById() throws Exception {
        projectTaskService.removeProjectById(USER_ID, "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testProjectBadRemoveProjectById() throws Exception {
        projectTaskService.removeProjectById(USER_ID, "123");
    }

    @Test
    public void testUnbindTaskFromProject() throws Exception {
        for (@NotNull final Task task : bindedTasks) {
            assertEquals(project.getId(), task.getProjectId());
            projectTaskService.unbindTaskFromProject(USER_ID, project.getId(), task.getId());
            @NotNull final Task actualTask = taskRepository.findOneById(task.getId());
            assertNull(actualTask.getProjectId());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject(USER_ID, "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject(USER_ID, "123", "");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testBadUserUnbindTaskFromProject() throws Exception {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }
}
