package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connectionService);

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private static final List<User> userList = new ArrayList<>();

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            user.setPasswordHash("123" + i);
            userRepository.add(user);
            userList.add(user);

            for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                @NotNull final Project project = new Project();
                project.setName("Project name: " + j);
                project.setDescription("Project description: " + j);
                project.setUserId(user.getId());
                projectRepository.add(project);

                for (int a = 0; a < NUMBER_OF_ENTRIES; a++) {
                    @NotNull final Task task = new Task();
                    task.setName("Task name: " + a);
                    task.setDescription("Task description: " + a);
                    task.setUserId(user.getId());
                    task.setProjectId(project.getId());
                    taskRepository.add(task);
                }
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        userService.clear();
        projectRepository.clear();
        taskRepository.clear();
        userList.clear();
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull List<User> userListActual = userRepository.findAll();
        assertEquals(userList, userListActual);

        userService.create("TEST_USER1", "TEST_PASSWORD");
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 1, userListActual.size());

        userService.create("TEST_USER2", "TEST_PASSWORD", "test@email.ru");
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 2, userListActual.size());

        userService.create("TEST_USER3", "TEST_PASSWORD", Role.USUAL);
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 3, userListActual.size());
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreate() throws Exception {
        userService.create("", "TEST_PASSWORD");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreate() throws Exception {
        userService.create("LOGIN", "");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithEmail() throws Exception {
        userService.create("", "TEST_PASSWORD", "test@email.ru");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithEmail() throws Exception {
        userService.create("LOGIN", "", "test@email.ru");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithRole() throws Exception {
        userService.create("", "TEST_PASSWORD", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithRole() throws Exception {
        userService.create("LOGIN", "", Role.USUAL);
    }

    @Test
    public void testFindByEmail() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final User actualUser = userService.findByEmail(user.getEmail());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testEmptyEmailFindByEmail() throws Exception {
        userService.findByEmail("");
    }

    @Test
    public void testFindByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final User actualUser = userService.findByLogin(user.getLogin());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginFindByLogin() throws Exception {
        userService.findByLogin("");
    }

    @Test
    public void testIsEmailExist() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userService.isEmailExist(user.getEmail()));
            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
            assertFalse(userService.isEmailExist(""));
        }
    }

    @Test
    public void testIsLoginExist() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userService.isLoginExist(user.getLogin()));
            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
            assertFalse(userService.isLoginExist(""));
        }
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            user.setLocked(false);
            assertFalse(user.isLocked());
            userService.lockUserByLogin(user.getLogin());
            @NotNull final User actualUser = userRepository.findOneById(user.getId());
            assertTrue(actualUser.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyLockUserByLogin() throws Exception {
        userService.lockUserByLogin("");
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByEmail(user.getEmail());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByLogin(user.getLogin());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test
    public void testSetPassword() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final String oldHash = user.getPasswordHash();
            userService.setPassword(user.getId(), "STRONPASS12@qq" + user.getLogin());
            @NotNull final User actualUser = userRepository.findOneById(user.getId());
            assertNotEquals(actualUser.getPasswordHash(), oldHash);
        }
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            user.setLocked(true);
            assertTrue(user.isLocked());
            userService.unlockUserByLogin(user.getLogin());
            @NotNull final User actualUser = userRepository.findOneById(user.getId());
            assertFalse(actualUser.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyUnlockUserByLogin() throws Exception {
        userService.unlockUserByLogin("");
    }

    @Test
    public void testUserUpdate() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final String firstName = user.getFirstName() + "TEST";
            @NotNull final String lastName = user.getLastName() + "TEST";
            @NotNull final String middleName = user.getMiddleName() + "TEST";
            assertNotEquals(user.getFirstName(), firstName);
            assertNotEquals(user.getLastName(), lastName);
            assertNotEquals(user.getMiddleName(), middleName);
            userService.userUpdate(user.getId(), firstName, lastName, middleName);
            @NotNull final User actualUser = userRepository.findOneById(user.getId());
            assertEquals(firstName, actualUser.getFirstName());
            assertEquals(lastName, actualUser.getLastName());
            assertEquals(middleName, actualUser.getMiddleName());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdUserUpdate() throws Exception {
        userService.userUpdate("", "firstName", "lastName", "middleName");
    }
}
