package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws SQLException;

    @NotNull
    M update(@NotNull String id, @NotNull M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    List<M> findAll() throws SQLException;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws SQLException;

    @NotNull
    M findOneById(@NotNull String id) throws AbstractEntityNotFoundException, SQLException;

    @NotNull
    M remove(@NotNull M model) throws SQLException;

    @NotNull
    M removeById(@NotNull String id) throws AbstractEntityNotFoundException, SQLException;

    void clear() throws SQLException;

    int getSize() throws SQLException;

    boolean existById(@NotNull String id) throws SQLException;

}
