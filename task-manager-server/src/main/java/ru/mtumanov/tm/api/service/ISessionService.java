package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.model.Session;

public interface ISessionService extends IUserOwnerService<Session> {

}
