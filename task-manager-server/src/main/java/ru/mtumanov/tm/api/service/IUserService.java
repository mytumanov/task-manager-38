package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

import java.sql.SQLException;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws AbstractException, SQLException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException, SQLException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws AbstractException, SQLException;

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException, SQLException;

    @NotNull
    User findByEmail(@NotNull String email) throws AbstractException, SQLException;

    @NotNull
    User removeByLogin(@NotNull String login) throws AbstractException, SQLException;

    @NotNull
    User removeByEmail(@NotNull String email) throws AbstractException, SQLException;

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password) throws AbstractException, SQLException;

    @NotNull
    User userUpdate(@NotNull String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws AbstractException, SQLException;

    boolean isLoginExist(@NotNull String login) throws SQLException;

    boolean isEmailExist(@NotNull String email) throws SQLException;

    void lockUserByLogin(@NotNull String login) throws AbstractException, SQLException;

    void unlockUserByLogin(@NotNull String login) throws AbstractException, SQLException;

}