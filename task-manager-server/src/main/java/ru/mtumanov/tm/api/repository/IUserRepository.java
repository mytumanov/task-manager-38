package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException, SQLException;

    @NotNull
    User findByEmail(@NotNull String email) throws AbstractException, SQLException;

    boolean isLoginExist(@NotNull String login) throws SQLException;

    boolean isEmailExist(@NotNull String email) throws SQLException;

}
