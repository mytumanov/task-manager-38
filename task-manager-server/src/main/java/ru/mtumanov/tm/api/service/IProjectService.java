package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;

import java.sql.SQLException;

public interface IProjectService extends IUserOwnerService<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException, SQLException;

}
