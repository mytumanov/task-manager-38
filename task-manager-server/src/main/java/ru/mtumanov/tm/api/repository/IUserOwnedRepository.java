package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws SQLException;

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractEntityNotFoundException, SQLException;

    @NotNull
    M remove(@NotNull String userId, @NotNull M model) throws AbstractEntityNotFoundException, SQLException;

    @NotNull
    M removeById(@NotNull String userId, @NotNull String id) throws AbstractEntityNotFoundException, SQLException;

    void clear(@NotNull String userId) throws SQLException;

    int getSize(@NotNull String userId) throws SQLException;

    boolean existById(@NotNull String userId, @NotNull String id) throws SQLException;

}
