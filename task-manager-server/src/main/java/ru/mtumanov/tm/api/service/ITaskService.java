package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException, SQLException;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException, SQLException;

}
