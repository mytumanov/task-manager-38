package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    List<M> findAll() throws SQLException;

    @NotNull
    M add(@NotNull M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws SQLException;

    @NotNull
    M findOneById(@NotNull String id) throws AbstractException, SQLException;

    @NotNull
    M remove(@NotNull M model) throws SQLException;

    @NotNull
    M removeById(@NotNull String id) throws AbstractException, SQLException;

    void clear() throws SQLException;

}
