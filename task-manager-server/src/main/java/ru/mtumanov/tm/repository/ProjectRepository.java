package ru.mtumanov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.model.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Getter
    private final String tableName = "tm_project";

    public ProjectRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @Override
    @NotNull
    public Project add(@NotNull final String userId, @NotNull final Project model) throws SQLException {
        @NotNull final String sql = String.format("INSERT INTO %s (id, created, name, description, status, user_id) " + "VALUES (?, ?, ?, ?, ?, ?)"
                , getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().toString());
            statement.setString(6, userId);
            statement.executeUpdate();
            connection.commit();
            model.setUserId(userId);
        }
        return model;
    }

    @Override
    @NotNull
    public Project add(@NotNull final Project model) throws SQLException {
        add(model.getUserId(), model);
        return model;
    }

    @Override
    @NotNull
    public Collection<Project> add(@NotNull final Collection<Project> models) throws SQLException {
        for (Project model : models) {
            add(model);
        }
        return models;
    }

    @Override
    @NotNull
    public Project update(@NotNull final String id, @NotNull final Project model) throws SQLException {
        @NotNull final String sql = String.format("UPDATE %s SET name = ?, description = ?, status = ?, user_id = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().toString());
            statement.setString(4, model.getUserId());
            statement.setString(5, id);
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Override
    @NotNull
    protected Project fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setCreated(row.getTimestamp("created"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        return project;
    }

}
