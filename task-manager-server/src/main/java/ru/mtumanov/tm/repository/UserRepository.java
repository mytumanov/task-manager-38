package ru.mtumanov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    private final String tableName = "tm_user";

    public UserRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws UserNotFoundException, SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                if (result.next())
                    return fetch(result);
            }
        }
        throw new UserNotFoundException();
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws UserNotFoundException, SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                if (result.next())
                    return fetch(result);
            }
        }
        throw new UserNotFoundException();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws SQLException {
        try {
            findByLogin(login);
            return true;
        } catch (@NotNull final UserNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) throws SQLException {
        try {
            findByEmail(email);
            return true;
        } catch (@NotNull final UserNotFoundException e) {
            return false;
        }
    }

    @Override
    @NotNull
    protected User fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("mid_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @Override
    @NotNull
    public User add(@NotNull final User model) throws SQLException {
        @NotNull final String sql = String.format("INSERT INTO %s (id, login, password_hash, email, first_name, last_name, mid_name, role, locked) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getEmail());
            statement.setString(5, model.getFirstName());
            statement.setString(6, model.getLastName());
            statement.setString(7, model.getMiddleName());
            statement.setString(8, model.getRole().toString());
            statement.setBoolean(9, model.isLocked());
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Override
    @NotNull
    public Collection<User> add(@NotNull final Collection<User> models) throws SQLException {
        for (User user : models) {
            add(user);
        }
        return models;
    }

    @Override
    @NotNull
    public User update(@NotNull final String id, @NotNull final User model) throws SQLException {
        @NotNull final String sql = String.format("UPDATE %s SET login = ?, password_hash = ?, email = ?, first_name = ?, "
                + "last_name = ?, mid_name = ?, role = ?, locked = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPasswordHash());
            statement.setString(3, model.getEmail());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setString(7, model.getRole().toString());
            statement.setBoolean(8, model.isLocked());
            statement.setString(9, id);
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

}
