package ru.mtumanov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Getter
    private final String tableName = "tm_task";

    public TaskRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException, SQLException {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final List<Task> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE project_id = ? AND user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                while (result.next()) resultList.add(fetch(result));
            }
        }
        return resultList;
    }

    @Override
    @NotNull
    public Task add(@NotNull final String userId, @NotNull final Task model) throws SQLException {
        @NotNull final String sql = String.format("INSERT INTO %s (id, created, name, description, status, user_id, project_id) " + "VALUES (?, ?, ?, ?, ?, ?, ?)"
                , getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().toString());
            statement.setString(6, userId);
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
            connection.commit();
            model.setUserId(userId);
        }
        return model;
    }

    @Override
    @NotNull
    public Task add(@NotNull final Task model) throws SQLException {
        add(model.getUserId(), model);
        return model;
    }

    @Override
    @NotNull
    public Collection<Task> add(@NotNull final Collection<Task> models) throws SQLException {
        for (Task model : models) {
            add(model);
        }
        return models;
    }

    @Override
    @NotNull
    public Task update(@NotNull final String id, @NotNull final Task model) throws SQLException {
        @NotNull final String sql = String.format("UPDATE %s SET name = ?, description = ?, status = ?, user_id = ?, project_id = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().toString());
            statement.setString(4, model.getUserId());
            statement.setString(5, model.getProjectId());
            statement.setString(6, id);
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Override
    @NotNull
    protected Task fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setCreated(row.getTimestamp("created"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

}
