package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row) throws SQLException;

    @Nullable
    protected String getComporator(@NotNull final Comparator comparator) {
        if (comparator instanceof CreatedComparator)
            return "created";
        else if (comparator instanceof NameComparator)
            return "name";
        else if (comparator instanceof StatusComparator)
            return "status";
        else
            return null;
    }

    @Override
    @NotNull
    public abstract M add(@NotNull final M model) throws SQLException;

    @Override
    @NotNull
    public abstract Collection<M> add(@NotNull final Collection<M> models) throws SQLException;

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException {
        clear();
        return add(models);
    }

    @Override
    @NotNull
    public List<M> findAll() throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql);
             @NotNull final ResultSet result = statement.executeQuery()) {
            while (result.next()) resultList.add(fetch(result));
        }
        return resultList;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getComporator(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql);
             @NotNull final ResultSet result = statement.executeQuery()) {
            while (result.next()) resultList.add(fetch(result));
        }
        return resultList;
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String id) throws AbstractEntityNotFoundException, SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                if (!result.next()) throw new EntityNotFoundException();
                return fetch(result);
            }
        }
    }

    @Override
    @NotNull
    public M remove(@NotNull final M model) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String id) throws AbstractEntityNotFoundException, SQLException {
        @NotNull final M model = findOneById(id);
        remove(model);
        return model;
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
            connection.commit();
        }
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet result = statement.executeQuery(sql)
        ) {
            result.next();
            return result.getInt("count");
        }
    }

    @Override
    public boolean existById(@NotNull final String id) throws SQLException {
        try {
            findOneById(id);
            return true;
        } catch (AbstractEntityNotFoundException e) {
            return false;
        }
    }

}
