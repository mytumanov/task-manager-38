package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.api.service.IService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public M add(@NotNull final M model) throws SQLException {
        return repository.add(model);
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) throws SQLException {
        return repository.add(models);
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException {
        return repository.set(models);
    }

    @Override
    public void clear() throws SQLException {
        repository.clear();
    }

    @Override
    @NotNull
    public List<M> findAll() throws SQLException {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws SQLException {
        if (comparator == null)
            return findAll();
        return repository.findAll(comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull String id) throws AbstractException, SQLException {
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @NotNull
    public M remove(@NotNull M model) throws SQLException {
        return repository.remove(model);
    }

    @Override
    @NotNull
    public M removeById(@NotNull String id) throws AbstractException, SQLException {
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.removeById(id);
    }

}
