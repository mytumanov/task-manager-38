package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IUserOwnedRepository;
import ru.mtumanov.tm.api.service.IUserOwnerService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    protected AbstractUserOwnedService(@NotNull R repository) {
        super(repository);
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator)
            throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (comparator == null)
            return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    @NotNull
    public M remove(@NotNull final String userId, @NotNull final M model) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        return repository.remove(userId, model);
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (id.isEmpty())
            throw new IdEmptyException();
        return repository.existById(userId, id);
    }

    @Override
    public int getSize(@NotNull final String userId) throws AbstractException, SQLException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

}
