package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectShowByIdRs extends AbstractProjectRs {

    public ProjectShowByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectShowByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}