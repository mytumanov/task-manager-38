package ru.mtumanov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public abstract class AbstractProjectRs extends AbstractResultRs {

    @Nullable
    @Getter
    @Setter
    private Project project;

    protected AbstractProjectRs(@Nullable final Project project) {
        this.project = project;
    }

    protected AbstractProjectRs(@NotNull Throwable err) {
        super(err);
    }

}
