package ru.mtumanov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@NoArgsConstructor
public final class DataXmlSaveJaxbRq extends AbstractUserRq {

    public DataXmlSaveJaxbRq(@Nullable final String token) {
        super(token);
    }

}
