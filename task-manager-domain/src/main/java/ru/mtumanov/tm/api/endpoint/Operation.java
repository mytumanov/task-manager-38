package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.AbstractRq;
import ru.mtumanov.tm.dto.response.AbstractRs;

@FunctionalInterface
public interface Operation<RQ extends AbstractRq, RS extends AbstractRs> {

    @NotNull
    RS execute(@NotNull RQ request);

}
