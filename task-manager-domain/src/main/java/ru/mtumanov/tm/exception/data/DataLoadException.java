package ru.mtumanov.tm.exception.data;

public class DataLoadException extends AbstractDataException {

    public DataLoadException() {
        super("ERROR! Data load exception!");
    }

}
